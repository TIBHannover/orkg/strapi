FROM node:16-alpine

RUN apk update && apk add --no-cache build-base gcc autoconf automake zlib-dev libpng-dev nasm bash vips-dev

# Make environment configurable. Default to "production".
ARG environment=production

WORKDIR /app

COPY ./package.json ./
COPY ./yarn.lock ./

# Install as if in "production" environment. Required for other environments.
# Also drop the cache to save space. (Not needed for build.)
RUN yarn install --production=true && yarn cache clean

COPY . .

ENV NODE_ENV $environment

EXPOSE 1337

CMD ["./entrypoint.sh"]
