# Strapi headless CMS 

The repo contains the code of Strapi CMS. [See the Wiki](https://gitlab.com/TIBHannover/orkg/strapi/-/wikis/home) for more information about how and where the different collection types are used.

# Running

If you just want to run the service, Docker compose is the easiest option. It runs both Strapi and Postgres. Just run:

```docker compose up -d```

This runs Strapi in `production` mode, meaning that the API cannot be changed. 
## For development 

1. Ensure you setup a Postgres database (or use Docker compose to only start Postgres `docker compose up -d postgres`)

2. Copy .env file and enter the correct database details

``` cp .env.example .env```

2. Install the dependencies 

```yarn install```

3. Start the server

```yarn develop```

## Getting started 

By default, the database doesn't contain any data. First, you have to create an admin account via http://localhost:1337. Afterwards, you have to specify the permissions of the API endpoints (to make them public). Do this as follows:

1. Sign in as administrator in Strapi
2. Go to Settings -> USERS & PERMISSIONS PLUGIN -> Roles -> Public
3. Select 'find' and 'findOne' the content types you want to make public.

Now you are ready to add data to Strapi. 

_For using the CMS in the [ORKG frontend](https://gitlab.com/TIBHannover/orkg/orkg-frontend/), set the `REACT_APP_CMS_URL` variable to `http://localhost:1337/api/` in the `.env` file in the frontend repository._
