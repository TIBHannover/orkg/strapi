'use strict';

/**
 * about-page-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::about-page-category.about-page-category');