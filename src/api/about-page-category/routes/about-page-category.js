'use strict';

/**
 * about-page-category router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::about-page-category.about-page-category');