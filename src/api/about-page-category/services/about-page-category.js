'use strict';

/**
 * about-page-category service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::about-page-category.about-page-category');