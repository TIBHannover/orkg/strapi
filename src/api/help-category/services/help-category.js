'use strict';

/**
 * help-category service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::help-category.help-category');