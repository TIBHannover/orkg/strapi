'use strict';

/**
 * home-alert controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::home-alert.home-alert');