'use strict';

/**
 * home-alert router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::home-alert.home-alert');