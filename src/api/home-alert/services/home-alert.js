'use strict';

/**
 * home-alert service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::home-alert.home-alert');