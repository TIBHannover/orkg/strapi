'use strict';

/**
 * sparql-example controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::sparql-example.sparql-example');
