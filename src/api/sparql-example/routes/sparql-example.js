'use strict';

/**
 * sparql-example router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::sparql-example.sparql-example');
