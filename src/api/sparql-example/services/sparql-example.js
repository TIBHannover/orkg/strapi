'use strict';

/**
 * sparql-example service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::sparql-example.sparql-example');
